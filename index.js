const express = require("express");

const app = express();

app.get("/", function (req, res){
    res.send("Seja bem vindo ao meu app");
});

app.get("/sobre", function(req, res){
    res.send("Bem-vindo ao meu blog");
});

app.listen(80, function(){
    console.log("Servidor rodando na url http://localhost:80");
}); //Tem que ser a ultima mensagem

